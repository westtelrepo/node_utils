// this is the javascript required by the main layout. The main layout DOES NOT include this
// automatically, you will need to include it in your build process.
// Also this is dependent on jQuery. Works with v1, hopefully works with v2-v3 as well.
//
// TODO: a proper requestAnimationFrame polyfill
$(function() {
	var header = $('#header');
	$('#icon-menu').click(function() {
		header.toggleClass('show-menu');
	});

	var $wrapper = $('#wrapper');
	var $push = $('#main-push');
	var $footer = $('#main-footer');
	// -1000 is always at least 2 away from a positive value (which height needs to be)
	var old_height = -1000;
	function fix() {
		var diff = $(window).height() - $wrapper.height() + $push.height() - $footer.height();
		//var diff = parseFloat($('body').css('height')) - parseFloat($wrapper.css('height')) + parseFloat($push.css('height')) - parseFloat($footer.css('height'));
		var new_height = diff > 0 ? diff : 0;
		// we don't change it until it is at least 2 pixels different... at certain heights
		// it turns into seizure mode because of rounding issues
		// TODO: why?
		if (Math.abs(new_height - old_height) > 2) {
			$push.height(new_height);
			//$push.css('height', new_height + 'px');
			old_height = new_height;
		}
	}

	if (window.requestAnimationFrame) {
		window.requestAnimationFrame(function fixalways() {
			fix();
			window.requestAnimationFrame(fixalways);
		});
	} else
		setInterval(fix, 50);
});

'use strict';
const assert = require('assert');
const co = require('co');

const module_map = new WeakMap();

module.exports = function(pg_promise) {
	if (pg_promise.$config && pg_promise.$config.pgp)
		pg_promise = pg_promise.$config.pgp;

	if (module_map.has(pg_promise))
		return module_map.get(pg_promise);

	const ret = {};

	// BEGIN ISOLATION LEVEL SERIALIZABLE, READ ONLY, DEFERRABLE;
	ret.tmSerializableRODeferrable = new pg_promise.txMode.TransactionMode({
		tiLevel: pg_promise.txMode.isolationLevel.serializable,
		readOnly: true,
		deferrable: true
	});
	// BEGIN ISOLATION LEVEL SERIALIZABLE, READ WRITE;
	ret.tmSerializableRW = new pg_promise.txMode.TransactionMode({
		tiLevel: pg_promise.txMode.isolationLevel.serializable,
		readOnly: false
	});

	// this is a helper to retry a transaction on serialization failure
	ret.transactor = co.wrap(function*(db, f) {
		assert(f.txMode, 'no txMode on f');
		assert(f.txMode instanceof pg_promise.txMode.TransactionMode, 'txMode wrong type');
		let error;
		for (let i = 0; i < 3; i++) {
			try {
				return yield db.tx(f);
			} catch(e) {
				error = e;
				if (e instanceof Error && (e.code === '40001' || e.code === '40P01'))
					;
				else
					break;
			}
		}
		throw error;
	});

	Object.freeze(ret);
	module_map.set(pg_promise, ret);
	return ret;
};


'use strict';
const assert = require('assert');
const co_express = require('co-express');
const cookie_parser = require('cookie-parser');
const express_session = require('express-session');
const path = require('path');

const connect_pg_simple = require('connect-pg-simple')(express_session);

const wconfig = require('/home/westtel/westtel_global_config');

exports.install_sessions = function(app, options) {
	assert(options.db, 'install_sessions requires pg-promise db connection');

	const db = options.db;
	const mydb = require('./db')(options.db['$config'].pgp);
	app.locals.wconfig = {
		accountsuri: wconfig.accountsuri,
		reportsuri: wconfig.reportsuri,
		phonebookuri: wconfig.phonebookuri
	};

	const express_session_store = new connect_pg_simple({
		conString: {application_name: 'westtel_sessions'},
		ttl: 1200,
		pruneSessionInterval: 'pruneSessionInterval' in options ? options.pruneSessionInterval : false
	});

	app.use(cookie_parser(wconfig.cookieSecret));

	app.use(express_session({
		store: express_session_store,
		secret: wconfig.cookieSecret,
		resave: false,
		saveUninitialized: false,
		cookie: {secure: true, domain:'.westtel.com'},
		name: wconfig.sessionCookieName,
	}));

	app.use(co_express(function*(req, res, next) {
		if (req.session.user !== undefined) {
			const uid = req.session.user.uid;
			res.locals.user = {uid};
			function* get(t) {
				return yield t.oneOrNone(
`SELECT username, timezone, superadmin, data->\'force_password_change\' AS force_password_change,
	disp_name
FROM users WHERE uid = $1`, uid);
			}
			get.txMode = mydb.tmSerializableRODeferrable;

			const result = yield mydb.transactor(db, get);

			if (result) {
				if (!result.disp_name)
					result.disp_name = 'Display Name unset for ' + result.username;
				Object.assign(res.locals.user, result);
				next();
			} else {
				// TODO: implement these...
				yield destroy_session(req.session);
				to_login(req, res);
			}
		} else
			next();
	}));

	app.locals.layout = path.join(__dirname, 'views/layouts/main.handlebars');
}

exports.check_auth = function(options) {
	return function(req, res, next) {
		function disallow(redirect_url) {
			if (options.error) {
				const e = new Error('Unauthorized');
				if (options.error.mark)
					e[options.error.mark] = true;
				next(e);
			} else {
				if (redirect_url)
					res.redirect(redirect_url)
				else {
					const url = req.protocol + '://' + req.get('host') + req.originalUrl;
					res.redirect(wconfig.accountsuri + '/login?redirect=' + encodeURIComponent(url));
				}
			}
		}

		if (!req.session.user)
			disallow();
		else if ((options.superuser || options.superadmin) && !res.locals.user.superadmin)
			disallow();
		else if (!options.no_change_password_redirect && res.locals.user.force_password_change)
			disallow(wconfig.accountsuri + '/change_password');
		else
			next();
	};
}

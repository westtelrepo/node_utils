'use strict';
const path = require('path');

// pass express app & top-level express.static
exports.install_favicon = function(app, _static) {
	app.use('/favicon.ico', _static(path.join(__dirname, 'favicon.ico')));
}
